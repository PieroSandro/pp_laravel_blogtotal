<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\PostController;

/*
Route::get('', function () {
    return "hola admin";
});*/

Route::get('', [HomeController::class,'index'])->name('admin.home');

//la url empieza con 'categories', y las rutas empiezan con 'admin.categories'
Route::resource('categories',CategoryController::class)->names('admin.categories');
//min:11_16

//Crud de etiqetas
Route::resource('tags',TagController::class)->names('admin.tags');

Route::resource('posts',PostController::class)->names('admin.posts');
