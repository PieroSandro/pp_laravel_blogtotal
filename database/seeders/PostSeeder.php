<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Post;

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts=Post::factory(300)->create();
        foreach($posts as $post){
            Image::factory(1)->create([
                'imageable_id'=>$post->id,
                'imageable_type'=>Post::class
            ]);
            //lo de abajo es de la relacion post y tag
            $post->tags()->attach(
                [
                    rand(1,4),
                    rand(5,8)
                ]
            );//en attach ira el id de tags, habran 2 etiquestas para cada post
        }
    }
}
